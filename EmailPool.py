#App library
import configparser
import socket
import sys

#Database library
import mysql.connector

#Email library
import smtplib
from email.mime.text import MIMEText

#SELECT `idEmail`,`dateAdd`,`AddresTo`,`asunto`,`text` FROM `email_queue` ORDER BY priority  DESC 




#Loading Module configs
config = configparser.ConfigParser()
config.read('EmailPool.conf')
try:
    #Module Configs
    PORT = int(config['DEFAULT']['PORT'])
    #Mysql Credentials
    MYSQL_USERNAME = config['MYSQL']['MYSQL_USERNAME']
    MYSQL_PASSWORD = config['MYSQL']['MYSQL_PASSWORD']
    MYSQL_DATABASE = config['MYSQL']['MYSQL_DATABASE']
    MYSQL_HOST = config['MYSQL']['MYSQL_HOST']
    #Email Service Credentials
    EMAIL_HOST = config['SMTP']['EMAIL_HOST']
    EMAIL_PORT = config['SMTP']['EMAIL_PORT']
    EMAIL_USERNAME = config['SMTP']['EMAIL_USERNAME']
    EMAIL_PASSWORD = config['SMTP']['EMAIL_PASSWORD']

except:
    print("# FATAL ERROR - Error al obtener los datos del config, posible mala configuracion")
    exit()





def main():
    #Socket Creation
    s = socket.socket()   
    s.bind(('127.0.0.1', PORT))
    #Only one client
    s.listen(1)

    while True:
        print(f"# Listening")
        #Client
        c, address = s.accept()
           #Database connection
        bd_sv = mysql.connector.connect(user=MYSQL_USERNAME, password=MYSQL_PASSWORD,
                                    host=MYSQL_HOST,
                                    database=MYSQL_DATABASE)
        cursor = bd_sv.cursor()
        # Get user input and send it to the client after encoding
        print(f"# Connection recived {address}")

        cursor.execute("SELECT `idEmail`,`dateAdd`,`AddresTo`,`asunto`,`text` FROM `email_queue` ORDER BY priority  DESC")
        email_list = cursor.fetchall()

        if(not(email_list)):
            print("# No email in the queue")
            c.close()
            continue


        email_sv = smtplib.SMTP_SSL(EMAIL_HOST,EMAIL_PORT)
        email_sv.ehlo_or_helo_if_needed()
        email_sv.login(EMAIL_USERNAME,EMAIL_PASSWORD)

        email_delete_query = ""
        
        for email in email_list:
            #Sending Mails
            print(f'# Seding email N°{email[0]} to:{email[2]}')
            to_address = email[2]
            subject = email[3]
            content = email[4]
            message = MIMEText(content)
            message['Subject'] = subject
            message['From'] = EMAIL_USERNAME
            message['To'] = to_address
            email_sv.sendmail(EMAIL_USERNAME, to_address,message.as_string())
            cursor.execute(f"DELETE FROM `email_queue` WHERE `idEmail`={email[0]} ; \n")
        email_sv.quit()
        print("# All emails sended deleting from the database")
        bd_sv.commit()
        
        print("# Deleted correctly")
        bd_sv.close()
        c.close()
            
           
#Was this file executed
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("# Ending Process")
        sys.exit()

